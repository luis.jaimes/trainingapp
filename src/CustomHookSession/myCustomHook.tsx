import AsyncStorage from "@react-native-async-storage/async-storage";
import { useEffect, useState } from "react"

export interface ITask {
    title: string;
}

const temporalDataHook = () => {
    console.log("soy el inicio del customHook");
    
    const [tasks, setTasks] = useState<ITask[]>([])

    useEffect(()=>{
        (async ()=>{
            console.log("soy la función selfinvoke");
            
            const myItems = await AsyncStorage.getItem("mytasks")
            console.log({myItems});
            const initialTasks: ITask[] = myItems ? JSON.parse(myItems) : []
            console.log({initialTasks});
            setTasks(initialTasks)
        })()
    }, [])

    const addNewTask = (task:ITask) => {
        setTasks([...tasks, task])
        AsyncStorage.setItem("mytasks", JSON.stringify([...tasks, task])).then(()=>{
            console.log("task saved");
            
        })
    }

    const removeTask = (task:ITask) => {
        const taskFiltered = tasks.filter((a)=> a.title !== task.title)
        setTasks(taskFiltered)
        AsyncStorage.setItem("mytasks", JSON.stringify(taskFiltered)).then(()=>{
            console.log("task removed");
            
        })
    }

    return {
        tasks,
        addNewTask,
        removeTask
    }
}

export {
    temporalDataHook
}