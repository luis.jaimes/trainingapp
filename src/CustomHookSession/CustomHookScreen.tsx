import React from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import { ITask, temporalDataHook } from './myCustomHook';

interface ISectionProps {
    title: string;
    itemNumber: number;
    removeTask: (task: ITask) => void
  }
  
  function Section({title, itemNumber, removeTask}: ISectionProps): JSX.Element {
    const backgroundColor = (itemNumber % 2 === 0) ? "orange" : "red" ;
    return (
        <TouchableOpacity onPress={()=>removeTask({title})}>
            <View style={[styles.sectionContainer, {backgroundColor}]}>
                <Text
                style={[
                    styles.sectionTitle,
                    { color: Colors.black }]}>
                {title}
                </Text>
            </View>
        </TouchableOpacity>
    );
  }

  
function CustomHookScreen(): JSX.Element {
    const { addNewTask, tasks, removeTask } = temporalDataHook()
    const isDarkMode = useColorScheme() === 'dark';
    const [text, onChangeText] = React.useState('');
    const backgroundStyle = {
      backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
    };

    const onSubmit = () => {
        addNewTask({title: text})
        onChangeText("")
    }
  

    return (
        <View style={{ flexDirection:"column", backgroundColor:"orange", flex: 1}}>
            <View>
                <TextInput
                    style={styles.input}
                    onChangeText={onChangeText}
                    value={text}
                />
                <TouchableOpacity onPress={onSubmit}>
                    <Text>Save my task</Text>
                </TouchableOpacity>
            </View>
            <ScrollView
            style={[backgroundStyle]}>
            { tasks.map((a,b) => <Section removeTask={removeTask} title={a.title} itemNumber={b} key={`key-${b}`} />)}
            </ScrollView>
      </View>
    );
  }

const styles = StyleSheet.create({
    sectionContainer: {
      marginTop: 5,
      paddingHorizontal: 24,
    },
    sectionTitle: {
      fontSize: 24,
      fontWeight: '600',
    },
    sectionDescription: {
      marginTop: 8,
      fontSize: 18,
      fontWeight: '400',
    },
    highlight: {
      fontWeight: '700',
    },
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        padding: 10,
      },
  });

  export { CustomHookScreen }